package app;

import app.abstraction.SmartDeviceManager;
import app.abstraction.refined_abstractions.AndroidSmartDeviceManager;
import app.implementation.implementors.SmartBulb;

public class Main {

//    Client Code example
    public static void main(String[] args) {
        SmartDeviceManager smartDeviceManager = new AndroidSmartDeviceManager(new SmartBulb());

        smartDeviceManager.turnOn();
    }
}
