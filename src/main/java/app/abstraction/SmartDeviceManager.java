package app.abstraction;

import app.implementation.SmartDevice;

public abstract class SmartDeviceManager {
    private SmartDevice smartDevice;

    protected SmartDeviceManager(SmartDevice smartDevice) {
        this.smartDevice = smartDevice;
    }

    public abstract void turnOn();

    public SmartDevice getSmartDevice() {
        return smartDevice;
    }

}
