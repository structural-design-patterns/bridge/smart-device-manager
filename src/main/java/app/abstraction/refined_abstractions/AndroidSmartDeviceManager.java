package app.abstraction.refined_abstractions;

import app.abstraction.SmartDeviceManager;
import app.implementation.SmartDevice;

public class AndroidSmartDeviceManager extends SmartDeviceManager {
    public AndroidSmartDeviceManager(SmartDevice smartDevice) {
        super(smartDevice);
    }

    @Override
    public void turnOn() {
        SmartDevice smartDevice = getSmartDevice();
        smartDevice.turnOn();
    }
}
