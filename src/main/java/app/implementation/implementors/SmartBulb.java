package app.implementation.implementors;

import app.implementation.SmartDevice;

public class SmartBulb implements SmartDevice {

    @Override
    public void turnOn() {
        System.out.println("The bulb is emitting light!");
    }
}
